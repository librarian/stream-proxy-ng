package main

import (
	"fmt"
	"net/http"
	"os"

	"codeberg.org/librarian/stream-proxy-ng/handlers"
)
func main() {
	http.HandleFunc("/stream/", handlers.HandleStream)
	http.HandleFunc("/live/", handlers.HandleLive)

	port := "3001"
	if os.Getenv("PORT") != "" {
		port = os.Getenv("PORT")
	}
	fmt.Println("Listening on " + os.Getenv("ADDRESS") + ":" + port)
	http.ListenAndServe(os.Getenv("ADDRESS") + ":" + port, nil)
}