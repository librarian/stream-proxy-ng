package sproxy

import (
	"fmt"
	"net/http"

	"codeberg.org/librarian/stream-proxy-ng/handlers"
)

func NewProxy(addr string) {
	http.HandleFunc("/stream/", handlers.HandleStream)
	http.HandleFunc("/live/", handlers.HandleLive)

	fmt.Println("Listening on " + addr)
	http.ListenAndServe(addr, nil)
}